from threading import Thread
import time

class Patience(Thread):
    """
    A class which will simply do something ("what") on something ("args") and
    something else ("kwargs") but return if it takes more than "seconds"
    seconds.
    """
    def __init__(self, seconds, what, args=[], kwargs={}):
        super(Patience, self).__init__()
        self.done = False
        self.error = None
        self.what = what
        self.args = args
        self.kwargs = kwargs
        self.seconds = seconds
    
    def go(self, logger=None):
        self.start()
        self._logger = logger
        for i in range(self.seconds):
            if self.error:
                return False
            time.sleep(1)
        if self.done:
            return (self._res,)
        else:
            return False
    
    def run(self):
        try:
            self._res = self.what(*self.args, **self.kwargs)
            self.done = True
        except:
            self.error = True
            if self._logger:
                self._logger.warning("Error")
            import traceback
            traceback.print_exc()
            self.done = False

