import os, sys
from xml.dom.minidom import parse, Element
from xdg import BaseDirectory

########################## CONFIGURATION #######################################
# The path of the directory containing the current executable - for running
# without installing:
not_installed_dir = sys.path[0]

if os.path.exists(not_installed_dir + '/stuff/reachme.svg'):
    STUFF_DIR = not_installed_dir + '/stuff'
    LOCALE_DIR = not_installed_dir + '/locale'
else:
    for directory in [sys.prefix, sys.prefix + '/local']:
        installed_root_dir = directory + '/share'
        if os.path.exists(installed_root_dir + '/reachme/stuff'):
            STUFF_DIR = installed_root_dir + '/reachme/stuff'
            LOCALE_DIR = installed_root_dir + '/locale'
            break

import gettext, locale

APP = 'reachme'

gettext.install(APP, localedir=LOCALE_DIR)

# For gtk.Builders:
locale.bindtextdomain(APP, LOCALE_DIR)

########################## END OF CONFIGURATION ################################

############################### USER DIRS ######################################

data_dir = BaseDirectory.xdg_data_dirs[0]
DATA_DIR = os.path.join( data_dir, 'reachme' )
if not os.path.exists( DATA_DIR ):
    os.mkdir( DATA_DIR )

KEYPAIRS_DIR = os.path.join( DATA_DIR, 'keypairs' )
if not os.path.exists( KEYPAIRS_DIR ):
    os.mkdir( KEYPAIRS_DIR )

config_dir = BaseDirectory.xdg_config_dirs[0]
CONFIG_DIR = os.path.join( config_dir, 'reachme' )
if not os.path.exists( CONFIG_DIR ):
    os.mkdir( CONFIG_DIR )

SSH_DIR = os.path.join(os.environ.get('HOME'), '.ssh')

######################## END OF USER DIRS ######################################


class Configuration(dict):
    def __init__(self, xml_file):
        doc = parse( xml_file )
        root = doc.childNodes[0]
        print(root)
        for ch in root.childNodes:
            if ch.__class__ is Element:
                for ch2 in ch.childNodes:
                    if ch2.__class__ is Element:
                        self[ch2.tagName] = ch2.childNodes[0].data.strip()
                        print (ch2.tagName)

