#!/usr/bin/python

from distutils.core import setup
from os import listdir

dist = setup(name='reachme',
      version='0.6.7',
      description='Simple tool for remote access to computers',
      license='GPL v3',
      author='Pietro Battiston',
      author_email='me@pietrobattiston.it',
      scripts=['reachme', 'reachme-light', 'reachthem', 'enable_vnc_access'],
      packages=['libreachme', 'libreachthem'],
      data_files=[('share/applications', ['reachme.desktop', 'reachthem.desktop']),
                  ('share/reachme/stuff', ['stuff/reachme.svg', 
                                           'stuff/reachthem.svg',
                                           'stuff/UI_reachthem.glade']),
				  ('share/pixmaps', ['stuff/reachme.svg', 'stuff/reachthem.svg'])],
      classifiers=[
        'License :: OSI Approved :: GNU General Public License (GPL)',
        'Programming Language :: Python',
        ]    
     )

