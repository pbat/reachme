#! /usr/bin/python3

from urllib.request import urlopen
from subprocess import call, check_output
import os
import sys
import time
import traceback
from paramiko import SSHClient, SSHException
from libreachme.rforward import reverse_forward_tunnel
from libreachme.threading_utils import Patience
from libreachme.ssh_utils import prepare_client
from threading import Thread
from xdg import BaseDirectory
import getpass
import socket
import datetime

import logging
logger = logging.getLogger("reachme")
logger.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
logger.addHandler(ch)

# Check connection every CHECK_EVERY seconds - can be overridden in config files.
CHECK_EVERY = 5

# Maybe "BaseDirectory.get_runtime_dir" is more appropriate, but it is not
# present on old python-xdg versions (it is in 0.25, but not in 0.19)
cache_home = BaseDirectory.xdg_cache_home
cache_path = os.path.join(cache_home, 'reachme')
lock_file_path = os.path.join(cache_path, 'lock')

# FIXME: use argparse
DAEMON = False
CONFIG_URL = None
for arg in sys.argv[1:]:
    if '--daemon' in sys.argv:
        DAEMON = True
    else:
        CONFIG_URL = arg

if DAEMON:
    # TODO: redirect output to log
    def log(*args, **kwargs):
        pass
    logger.log = log
    logger.debug = log
    logger.info = log

class TimeStamper(object):
    def __init__(self, user, port, host):
        self._user = user
        self._port = port
        self._host = host
        self._inited_remote = False
    
    def os_path_exists(self, sftp, path):
        try:
            sftp.stat(path)
            return True
        except IOError:
            return False
    
    def __init_remote__(self, sftp):
        home_path = (check_output(['ssh',
                                   f'{self._user}@{self._host}',
                                   'pwd']).decode('utf-8')
                                          .strip())

        self._timestamp_path = '%s/.cache/reachme/%s' % (home_path, self._port)
        
        base = os.path.dirname(self._timestamp_path)
        
        # Like os.makedirs, without os.makedirs and os.path.exists:
        if not self.os_path_exists(sftp, base):
            parts = base.split('/')
            prefix = '/'
            while parts:
                new_part = parts.pop(0)
                prefix = os.path.join(prefix, new_part)
                if not self.os_path_exists(sftp, prefix):
                    sftp.mkdir(prefix)
        
        self._inited_remote = True
    
    def stamp(self, client):
        sftp = client.open_sftp()
        if not self._inited_remote:
            self.__init_remote__(sftp)
        
        with sftp.open(self._timestamp_path, 'w') as f_stamp:
            f_stamp.write('%s from %s at %s\n' % (getpass.getuser(),
                                              socket.gethostname(),
                                              str(datetime.datetime.now())))
        

class CheckAlive(Thread):
    def __init__(self, client, check_every=CHECK_EVERY, check_func=None):
        super(CheckAlive, self).__init__()
        self.client = client
        self.stopped = False
        self.waiting_since = None
        self.stuck = False
        self._check_every = check_every
        self._check_func = check_func
        
        self.checker = CheckChecker(self)
        self.checker.start()
    
    def run(self):
        i = 0
        while not self.stopped:
            logger.debug("Check...")
            self.waiting_since = time.time()
            try:
                if self._check_func:
                    self._check_func(self.client)
                else:
                    self.client.exec_command("echo")
                logger.debug("... checked")
            except (SSHException, EOFError):
                logger.warning("... check failed!")
                self.is_stuck()
            self.waiting_since = None
            time.sleep(self._check_every)
            i += 1
    
    def stop(self):
        logger.debug("closing check...")
        self.stopped = True
        self.checker.stop()
        self.join()
        self.checker.join()
    
    def is_stuck(self):
        self.stuck = True
        self.client.close()
        self.stopped = True

class CheckChecker(Thread):
    def __init__(self, checked):
        super(CheckChecker, self).__init__()
        self.stopped = False
        self.done = False
        
        self.checked = checked
    
    def run(self):
        while not self.stopped:
            if self.checked.waiting_since:
                elapsed = time.time() - self.checked.waiting_since
                if elapsed > 10:
                    msg = "%s seconds elapsed, connection lost!" % elapsed
                    logger.warning(msg)
                    self.checked.is_stuck()
                    break
            
            time.sleep(3)
        self.done = True
    
    def stop(self):
        logger.debug("closing checker...")
        self.stopped = True
        if not self.done:
            self.join()
        logger.debug("... closed")

def quit(threads):
    logger.debug("Quit")
    for t in threads:
        t.stop()

def kill_old_tunnel(port, client):
    # Oh no... "-p" wants sudo
    tunnels_finder = "netstat -ntpl  | grep \"127.0.0.1:%s\"" % port
    stdin, stdout, stderr = client.exec_command(tunnels_finder)
    lines = stdout.readlines()
    if not lines:
        # No tunnel found
        return
    logger.debug("Found tunnels %s" % lines)
    
    processes_finder = "ps ux | grep ssh | grep notty"
    stdin, stdout, stderr = client.exec_command(processes_finder)
    lines = stdout.readlines()
    if len(lines)>1:
        logger.error("Sorry, too many processes found on the remote host, I "
                     "am unable to understand which is the abandoned tunnel.")
        logger.error(str(lines))
        raise NotImplementedError
    pid = lines[0].split()[1]
    logger.info("Killing old tunnel %s" % pid)
    client.exec_command("kill -9 %d" % pid)    

def download_config():
    if not CONFIG_URL:
        msg = ("No configuration file was found: to use reachme, please "
               "create one, or provide as argument an URL from which the "
               "connection info can be retrieved, as in:\n"
               "reachme http://example.com/reachmeinfo\n"
               "Reachme will then download info about host, username and port "
               "from http://example.com/reachmeinfo/X , with \"X\" replaced "
               "by \"IP\", \"uname\" and \"port\", respectively.")
        raise RuntimeError(msg)
    while True:
        conf = {'check_every' : CHECK_EVERY}
        try:
            f = urlopen('%s/ip' % CONFIG_URL)
            conf['IP'] = f.read().strip()
            logger.debug("IP %s" % conf['IP'])

            f = urlopen('%s/uname' % CONFIG_URL)
            conf['uname'] = f.read().strip()
            logger.debug("uname %s" % conf['uname'])

            f = urlopen('%s/port' % CONFIG_URL)
            conf['port'] = f.read().strip()
            logger.debug("port %s" % conf['port'])
            
            break
        
        except IOError:
            logger.warning("Unable to retrieve connection information, trying "
                           "again in 10 seconds (the error follows).")
            traceback.print_exc()
            time.sleep(10)
    return conf

def set_lock_file(lock_file_path):
    if not os.path.exists(cache_path):
        os.mkdir(cache_path)
    with open(lock_file_path, 'w') as lock_file:
        lock_file.write(str(os.getpid()))
    logger.debug("lock file set")

def remove_lock_file():
    os.remove(lock_file_path)

def check_running():
    if not os.path.exists(lock_file_path):
        logger.debug('Lock file does not exist, setting')
        set_lock_file(lock_file_path)
        return None
    
    with open(lock_file_path) as lock_file:
        pid = lock_file.read().strip()
    
    cmdline_path = '/proc/%s/cmdline' % pid
    
    if not os.path.exists(cmdline_path):
        # Stale file
        logger.debug('"%s" not found, setting' % cmdline_path)
        set_lock_file(lock_file_path)
        return None
    
    with open(cmdline_path) as cmdline_file:
        cmdline = cmdline_file.read()
    
    if 'reachme' not in cmdline:
        # Stale file
        logger.debug('"reachme" not in "%s", setting' % cmdline)
        set_lock_file(lock_file_path)
        return None
    
    return pid

def main():
    key_path = os.path.join(os.environ.get('HOME'), '.ssh/id_rsa.pub')

    if os.path.exists(key_path):
        logger.info("Found a key")
    
    paths = BaseDirectory.load_config_paths('reachme')
    force_config_paths = [os.path.join(path, 'force_config') for path in paths]
    force_config = [path for path in force_config_paths if os.path.exists(path)]
    if force_config:
        with open(force_config[0]) as fconf:
            conf = {'check_every' : CHECK_EVERY}
            conf.update(eval(fconf.read()))
    else:
        conf = download_config()

    while True:
        try:
            threads = []
            
            res = Patience(30, prepare_client, (conf['IP'], conf['uname'])).go()
            if res:
                client = res[0]
            else:
                logger.warning("Connection failed, retrying in few seconds...")
                time.sleep(5)
                continue
            
            
            transport = client.get_transport()
            transport.set_keepalive(20)
            
            check = CheckAlive(client,
                               check_every=int(conf['check_every']),
                               check_func=TimeStamper(conf['uname'],
                                                      conf['port'],
                                                      conf['IP']).stamp)
            check.start()
            threads.append(check)
            
            logger.info("Forwarding %s" % conf['port'])
            kill_old_tunnel(conf['port'], client)
            reverse_forward_tunnel(str(conf['port']), "localhost", 22, transport)
            logger.info("Tunnel closed")
            
            
            client.close()
            for t in threads:
                t.stop()
            time.sleep(5)
            logger.info("Restarting connection")
            continue
        
        except KeyboardInterrupt:
            quit(threads)
            break
        except:
            logger.warning("Error")
            traceback.print_exc()
            quit(threads)
            break

def app():
    pid = check_running()
    if pid:
        logger.debug("Found an already running instance (pid %s) - quitting" % pid)
    else:
        # Say that the daemon was not running:
        # FIXME: use logging, with specific level so that it is printed in --daemon mode
        print("reachme not running")
        try:    
            main()
        except:
            logger.warning("Error")
            traceback.print_exc()
            if not DAEMON:
                print("\nPress Enter to exit.")
                input()
        finally:
            remove_lock_file()

if __name__ == '__main__':
    main()
