import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

class Ui(object):
    def __init__(self, APP, filename):
        self._builder = Gtk.Builder()
        self._builder.set_translation_domain(APP)
        self._builder.add_from_file(filename)
    
    def __getattr__(self, attr_name):
        try:
            return object.__getattribute__(self, attr_name)
        except AttributeError:
            obj = self._builder.get_object(attr_name)
            if obj:
                self.obj = obj
                return obj
            else:
                raise AttributeError("no object named \"%s\" in the GUI."
                                     % attr_name)
    
    def connect_signals(self, target):
        self._builder.connect_signals( target )
    
    def get_value_as_string(self, attr_name):
        
        widget = getattr( self, attr_name )
        
        if hasattr( widget, "get_text" ):
            return widget.get_text()
        
        return str( int( widget.get_value() ) )
    
    def set_value_from_string(self, attr_name, value):
        
        widget = getattr( self, attr_name )
        
        if hasattr( widget, "set_value" ):
            widget.set_value( int( value ) )
        else:
            widget.set_text( value )
