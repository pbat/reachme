What is this?
-------------

**reachme** and **reachthem** are two python programs which simplify the operation of logging in to a remote machine without a public address.

Notice that in order to do so, a machine with public address is needed.

So here are the ingredients:

 * a machine **A** which cannot be reached from the net
 * a machine **B** which can be reached from the net
 * a machine **C** from which you want to reach **A**

If this sounds complicated, think to it as "LogMeIn except that you, not LogMeIn, provide **B**".

And this is how you use it:

  * **reachme** is ran on **A**
  * **reachthem** is ran on **C**, sees there is a connection to **A** available, and allows the user of **C** to connect to **A**.

There is nothing very new or complicated in this: it just uses SSH port forwarding, a classic (and pretty safe) tool. **reachme** and **reachthem** make the process just a bit simpler, in particular if the user of **A** is technically not very sophisticated.

Typical use cases
-----------------

  * **A** is a computer in your office which you always want to be able to reach from anywhere. Unfortunately, it is behind a NAT. **B** is a server of your company which instead has a public IP.
  * **A** is your boyfriend/girlfriend/mother/grandmother/father/grandfather/sister/brother's (half of these cases were personally tested by the author) computer, on which you installed Ubuntu some months ago. Now he/she needs to install a package, and he/she has no idea of how to do. **B** is your cheap VPS somewhere.

Here is how you use it, really
------------------------------

  * Install **reachme** (or **reachme-light**, if it is a server) on **A**, and create a file **~/.config/reachme/force_config** with content

```
  {'IP': 'example.com',
   'uname': 'myuser',
   'port': '1333',
   'check_every': '30'}
```

  * Install **reachthem** (and **reachme**, on which it depends) on **C** (replace "example.com" with the IP or address of **B**)
  * Set up SSH keys (click [here](https://www.debian.org/devel/passwordlessssh) if you don't know what this means) such that **C** is allowed passwordless access to **A**, and both **A** (with the username mentioned in the above configuration file, //which you will probably want to be an ad-hoc user, with limited rights!//) and **C** are allowed passwordless access to **B** (this is the only operation which must be done manually on **B**; things may change in the future).
  * (Tell your loved one to) start **reachme** on **A**
  * Start **reachthem** on **B**, and fill the fields **Address** (of **B**) and **Username** (on **B**) (as well as **Sshd port** if it is different than 22). Then retrieve the list of "**Available hosts**". **A** should appear: click on it, and then click **Open shell**


How to install
--------------

Download the above packages for Ubuntu/Debian, or clone the git repo:

```
  git clone https://gitlab.com/pbat/reachme.git
```

(both **reachme** and **reachthem** can be ran from the repo, without installing them).

How to run without user interaction
-----------------------------------

In order to reach a server at any time, you will presumably want to add a crontab line (with **crontab -e**) such as

```
    */10 * * * * reachme --daemon
```

What doesn't work
-----------------

Currently, probably screen access. Terminal, filesystem access and port forwarding should work.
