#!/usr/bin/python

from __future__ import print_function
import sys

from libreachthem.available_targets import available_targets

# FIXME: should show the file modification date, not (just) the date of the originator system

connection_info = sys.argv[1:]

if len(connection_info) not in (2, 3):
    print("Usage:\n\"./active_hosts username host port\"\ni.e.:\n"
          "./active_hosts pietro example.com 22\n\n"
          "\"port\" can be omitted if it is 22.")
    sys.exit(1)


uname = connection_info[0]
host = connection_info[1]
sshd_port = connection_info[2] if len(connection_info) == 3 else 22

connections = []

targets = list(available_targets(uname, host, sshd_port))

targets.sort(reverse=True, key=lambda t : (t.date, t.time))

fields = ('date', 'time', 'username', 'hostname')

for t in targets:
    print("\t".join([str(getattr(t, field)) for field in fields]))

