from paramiko import SSHClient, WarningPolicy
from threading import Thread
from .forward import Handler, ForwardServer
import os

def prepare_client(IP, uname, port=22, logger=None):
    if logger:
        logger.log(IP, uname, port, logger)
    client = SSHClient()
    client.load_system_host_keys()
    user_hostsfile = os.path.expanduser("~/.ssh/known_hosts")
    if os.path.exists(user_hostsfile):
        client.load_host_keys(user_hostsfile)
    client.set_missing_host_key_policy(WarningPolicy())
    if logger:
        logger.info("Connecting")
    client.connect(IP, port, username=uname)
    
    return client

def forward_tunnel(local_port, remote_host, remote_port, transport):
    # Identical to the function in "forward.py", except that it returns the
    # server rather than launching it

    # this is a little convoluted, but lets me configure things for the Handler
    # object.  (SocketServer doesn't give Handlers any way to access the outer
    # server normally.)
    class SubHander (Handler):
        chain_host = remote_host
        chain_port = remote_port
        ssh_transport = transport
    return ForwardServer(('', local_port), SubHander)
