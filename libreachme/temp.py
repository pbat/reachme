import os
from random import random
from stat import S_IWUSR, S_IRUSR, S_IXUSR

# Since paramiko.SSHClient reads keys from files only, we need to store them
# somewhere:
TEMPDIR = '/tmp/reachme%f' % random()
os.mkdir( TEMPDIR )
# Nobody should be able to look in the dir, containing a private key:
os.chmod( TEMPDIR, S_IRUSR | S_IWUSR | S_IXUSR )

def tempfile():
    return os.path.join( TEMPDIR, 'file_%f' % random() )
    
