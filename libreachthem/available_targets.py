#!/usr/bin/env python3

from __future__ import print_function
import os
import subprocess
import tempfile

# FIXME: should show the file modification date, not (just) the date of the originator system

REMOTEPATH = '{}/.cache/reachme'

class Target(object):
    def __init__(self, port):
        self.port = int(port)

def available_targets(username, host, port):

    home_path = (subprocess.check_output(['ssh', f'{username}@{host}', 'pwd'])
                           .decode('utf-8')
                           .strip())
    remotepath = REMOTEPATH.format(home_path)
    # FIXME: check if mounted
    mount_cmd = ['gio', 'mount', 'sftp://%s@%s:%s' % (username, host, port)]
    subprocess.call(mount_cmd)
    
    mount_cmd = ['gio', 'list', '-a', 'time',
                 'sftp://%s@%s:%s/%s' % (username, host, port, remotepath)]
    out = subprocess.check_output(mount_cmd)
        
    # Current version of gvfs-copy does not support recursive copy
    for l in out.splitlines():
        rem_port, mode, ftype, modified, access = (l.decode('utf-8')
                                                    .strip().split())
        mod_time = int(modified.split('=')[1])
        loc_handle, loc_path = tempfile.mkstemp("reachthem")

        cp_cmd = ['gio', 'copy',
                  'sftp://%s@%s:%s/%s/%s' % (username, host, port,
                                             remotepath, rem_port),
                  loc_path]
        subprocess.call(cp_cmd)
        t = Target(rem_port)
        with open(loc_path) as floc:
            content = floc.read()
            fields = content.split()
            try:
                t.username, _, t.hostname, _, t.date, t.time = fields
                t.time = mod_time
                yield t
            except ValueError:
                print(port, "Wrong format! \"%s\"" % content)
                continue
            finally:
                os.remove(loc_path)        

